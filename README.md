RAPID-utilities

RAPID is a high-level programming language used to control ABB industrial robots. I load here some RAPID modules I wrote, that may come handy for your projects or just for your learning process.

This repo contains modules for receiving files from a server on the network.

DISCLAIMER:
Keep in mind that sockets do not provide an authentication mechanism; anyone who can access the network can inject their own data and control the robot, **the feature is not secure. Use it at your own risk.**

INSTRUCTIONS:
Import all .mod and .sys files from this repository into a RAPID task.
Edit the IP and PORT contained in the file "local_networking.sys" according to the IP of your controller and of any desired port not busy.

On the other side, the server on the LAN must connect to those IP and PORT so edit the python files accordingly.