MODULE LAN_connect
!**********************************************************
!* Module name: LAN_connect
!**********************************************************
!* Description: calls ConnectionRoutine and start listening the client
!* Requires: this module requires the import of 
!*           1 local_networking.sys (find it on stacrux's repository RAPID-utils)
!*           2 string_utils.sys (find it on stacrux's repository RAPID-utils)
!* Notes: here are the conventions used for this communication process
!*        1 every communication must begin with the type of file, for example "mod" 
!*          allowed : mod, xml
!*        2 the second message of the communication must be the name of the file you want to create.
!*          I do not check for reserved words, just don't be dumb and use safe things
!*        3 the end of communication is represented by the string "END OF FILE"
!*          
!* Date:        Version:    Programmer:     Reason:
!* 23/3/2017    1.0         stacrux         
!**********************************************************


!**********************************************************
!* ROUTINE NAME: main
!**********************************************************
!*** name = main
!*** description = the main routine of this module; the main purpose is to open a socket communication 
!*** parameters = none
!*** errors = ERR_SOCK_CLOSED : a disconnection is raised untill this module where as a solution, the routine for connecting is issued again
!**********************************************************
    PROC main()
        
        TPErase;
        ! call the routine for creating a new connection
        ConnectionRoutine;
        ! after someone connected to us, we start retrieving data 
        sockStatus := SocketGetStatus( clientSocket );
        WHILE (sockStatus = SOCKET_CONNECTED) DO
               what_am_I_receiving;
               sockStatus := SocketGetStatus(clientSocket);
        ENDWHILE  
        !*********************MAIN ERROR HANDLER****************        
        ERROR
            IF ERRNO = ERR_SOCK_CLOSED THEN
                TPWrite "connection lost, reconnecting...";
                ConnectionRoutine;
                sockStatus := SocketGetStatus( clientSocket );
                WHILE (sockStatus <> SOCKET_CONNECTED) DO
                    ConnectionRoutine;
                    sockStatus := SocketGetStatus(clientSocket);
                ENDWHILE  
                RETRY; 
            ENDIF
    ENDPROC    

!**********************************************************
!* ROUTINE NAME: what_am_I_receiving
!**********************************************************
!*** description = this routine will receive a string through socket
!*** parameters = none
!*** assumptions = well formatted string (client must take care of this, you do not want the controller to format text, the less computation it does, the better)
!*** errors = n/d
!**********************************************************
    PROC what_am_I_receiving()
            ! reset the string
            ReceivedString := stEmpty;
            ! Receive the data
            SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
            ! just check the first string received and start the right procedure :) easy
            ! so remember to start the transmission on the other side with one of the following
            TEST toLowerCaseFUNC(ReceivedString)
                CASE ".mod" : fetchModule;
                CASE ".xml" : fetchXML;
                CASE ".txt" : fetchTXT;
                DEFAULT : SocketSend clientSocket, \Str:="unknown file";
            ENDTEST
    ENDPROC


ENDMODULE