MODULE LAN_FetchTXT
!**********************************************************
!* Module name: LAN_FetchTXT
!**********************************************************
!* Description: if you are sure you are receiving a .txt file, create it and write to it string by string, all received through socket
!* important note : in rapid, strings are 80 chars. in the client application you want to format the .txt file in the right way.
!*
!* additional notes : why are there modules based on the type and not just one single module, in which you create the file according to the first
!*                    string received?
!*                    because maybe in future you will want to manipulate what you receive and in this way you will always be in the
!*                    right section and you won't add an avalanche of "IF" or "TEST", checking the type :) 
!* Date:        Version:    Programmer:     Reason:
!* 23/3/2017    1.0         stacrux         
!**********************************************************    
    
!**********************************************************
!* ROUTINE NAME: fetchModule
!**********************************************************
!*** description = the main routine of this module; the main purpose is to open a socket communication 
!*** parameters = none
!**********************************************************
    PROC fetchTXT()
        VAR iodev txtFile;
        
        !inform the client that I did riceive something
        SocketSend clientSocket, \Str:="received mod";
        
        ! reset the string
        ReceivedString := stEmpty;
        
        ! Receive THE NAME of the file
        SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
        
        ! remember that we will retrieve strings untill we receive "END OF FILE"
        WHILE ReceivedString = "END OF FILE" DO
            TPWrite "you dumbass, nice try";
            SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
        ENDWHILE
        
        ! create the file, this will erase any existing file with the same name.type
        open "HOME:",\File:=ReceivedString,txtFile \Write;
        
        ! start reception of lines
        WHILE ReceivedString <> "END OF FILE" DO
            ! reset the string
            ReceivedString := stEmpty;
            ! Receive next line
            SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
            ! write it to the file
            IF ReceivedString <> "END OF FILE" THEN
                Write txtFile, ReceivedString;                
            ENDIF
            ! ack the other side
            SocketSend clientSocket,\Str:="line received";
        ENDWHILE
        
        close txtFile;
        SocketClose clientSocket;
        SocketClose serverSocket;
!****************************ERROR HANDLER******************        
        ERROR
            IF ERRNO = ERR_SOCK_CLOSED THEN
                TPWrite "connection lost, reconnecting...";
                ConnectionRoutine;
                sockStatus := SocketGetStatus( clientSocket );
                WHILE (sockStatus <> SOCKET_CONNECTED) DO
                    ConnectionRoutine;
                    sockStatus := SocketGetStatus(clientSocket);
                ENDWHILE  
                RETRY; 
            ENDIF
    ENDPROC

    
ENDMODULE