MODULE LAN_FetchXML
!**********************************************************
!* Module name: LAN_FetchXML
!**********************************************************
!* Description: if you are sure you are receiving a .xml file, create it and write to it string by string, all received through socket
!* important note : in rapid strings are 80 chars. in the client application you want to format the .xml file in the right way.
!* additional notes : why are there modules based on the type and not just one single module, in which you create the file according to the first
!*         string received?     because maybe in future you will want to manipulate what you receive and in this way you will always be in the
!*                              right section and you won't add an avalanche of "IF" or "TEST", checking the type :) 
!* Date:        Version:    Programmer:     Reason:
!* 23/3/2017    1.0         stacrux         
!**********************************************************    

!**********************************************************
!* ROUTINE NAME: fetchXML
!**********************************************************
!*** description = create a file in HOME:/ with the name received as first string through the socket and filled with all the remaining strings received
!***               until the string "END OF FILE" is received
!*** parameters = none
!**********************************************************
    PROC fetchXML()
        VAR iodev xmlFile;
        
        !inform the client that I did riceive something
        SocketSend clientSocket, \Str:="received mod";
        
        ! reset the string
        ReceivedString := stEmpty;
        
        ! Receive THE NAME of the file
        SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
        
        ! remember that we will retrieve strings untill we receive "END OF FILE"
        WHILE ReceivedString = "END OF FILE" DO
            TPWrite "you dumbass, nice try";
            SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
        ENDWHILE
        
        ! create the file, this will erase any existing file with the same name.type
        open "HOME:",\File:=ReceivedString,xmlFile \Write;
        
        ! start reception of lines
        WHILE ReceivedString <> "END OF FILE" DO
            ! reset the string
            ReceivedString := stEmpty;
            ! Receive next line
            SocketReceive clientSocket\Str:=ReceivedString\Time:=WAIT_MAX;
            ! write it to the file
            IF ReceivedString <> "END OF FILE" THEN
                Write xmlFile, ReceivedString;                
            ENDIF
            ! ack the other side
            SocketSend clientSocket,\Str:="line received";
        ENDWHILE
        
        close xmlFile;
        SocketClose clientSocket;
        SocketClose serverSocket;
!****************************ERROR HANDLER******************        
        ERROR
            IF ERRNO = ERR_SOCK_CLOSED THEN
                TPWrite "connection lost, reconnecting...";
                ConnectionRoutine;
                sockStatus := SocketGetStatus( clientSocket );
                WHILE (sockStatus <> SOCKET_CONNECTED) DO
                    ConnectionRoutine;
                    sockStatus := SocketGetStatus(clientSocket);
                ENDWHILE  
                RETRY; 
            ENDIF
    ENDPROC

ENDMODULE