MODULE this_is_an_example

	CONST string example := "hello world";
	
	PROC printExample()
		VAR num index := 0;
		TPWrite example;
	ENDPROC
	
ENDMODULE