import socket
import time
import os
import sys
from socket_utilities import get_my_ip_address
import command_line

string_lenght_rapid = 80
file_name, file_extension = os.path.splitext(command_line.args.file)

os.system('clear')
print ('********************************************************************************')
print ('********************************************************************************')
print ('********************************************************************************')
print ('***************************** ABB file forwarder *******************************')
print ('********************** this script is intended for python3 *********************')
print ('********************************************************************************')
print ('********************************************************************************')
print ('********************************************************************************')
print ('********************************************************************************')


if command_line.args.ip is  None:
	print ("hey : you did not provide any IP address for the controller "
			"I will use the default one")
	controller_ip = "192.168.125.1"
else:
	controller_ip = command_line.args.ip
if command_line.args.p is  None:
	print ("hey : you did not provide any port number for the connection\nyou can read the right one on the flex pendant")
	port = 9922
else:
	port = command_line.args.p

this_ip = get_my_ip_address()

print ("hey : take these useful infos\nyour IP : \t%s\nController's IP : \t%s\nController's port : \t%d"%(this_ip,controller_ip,port))
print ("if the connection is not happening, check the controller's socket implementation or simply turn it on")

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
client_socket.bind((this_ip,port))

while True:
	try:
		print ('connecting to the controller')
		print ('.')
		time.sleep(0.4)
		print ('.')
		time.sleep(0.4)
		print ('.')
		client_socket.connect((controller_ip,port))
	except OSError: 
		print ('there are problems connecting to the server') 
		print ('is it online?')
		print ('retrying in 5 seconds')
		time.sleep(5)
		continue
	else:
		print ('******************* connection established **********************')
		print ('********                                                *********')
		print ('********  [%s] <-----> [%s]       *********' %(controller_ip,this_ip))
		print ('********                 port : %d                    *********'%(port))
		print ('********                                                *********')
		print ('*****************************************************************')
		break



file_complete_uri = command_line.args.file

#send ".mod" to the controller and wait for the answer
client_socket.send(file_extension.encode())

data = client_socket.recv(string_lenght_rapid)
answer = data.decode()

if answer == "unknown file":
	print ("something went wrong")
	#close the socket
	client_socket.close()
	sys.exit(1)

#load every single line of the specified file into a list file_content
with open(file_complete_uri) as specified_file:
	#send specified_file.name to the controller
	client_socket.send(specified_file.name.encode())
	file_content = specified_file.readlines()


# now sending every line with a for, it will wait for an ack (the controller is sending a string back)
answer = "undefined"
for i in range(0,len(file_content)):
	# send the current line
	client_socket.send(file_content[i].encode())
	# wait for a reply
	data = client_socket.recv(string_lenght_rapid)
	answer = data.decode()
	
#the file is ended, inform the controller
client_socket.send("END OF FILE".encode())

#inform the user
print ("%s was sent without errors"%(file_name))
#close the socket
client_socket.close()