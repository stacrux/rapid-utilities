import argparse

parser = argparse.ArgumentParser(description="The script sends a file to an ABB controller on the lan where the application LAN_connect is running")

parser.add_argument('--ip', type=str,
                    help='the IP address of the controller that you want to contact')
parser.add_argument('--p', type=int,
                    help='the number of port where the controller is waiting')
parser.add_argument('--file', type=str,
                    help='path to the file you wish to forward')
args = parser.parse_args()
