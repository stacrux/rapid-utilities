import socket
# a method that opens a generic socket and try a connection, in order to generate
# a packet and retrieve local ip address
def get_my_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]